from django.urls import path, register_converter
from . import views
from . import converters

# реєстрація створеного конвертера
register_converter(converters.FourDigitYearConverter, "year4")


urlpatterns = [
    path("", views.TurnirHome.as_view(), name="home"),
    path("statistic/", views.statistic, name="statistic"),
    path("add_turnir/", views.AddTurnir.as_view(), name="add_turnir"),
    path("update_turnir/<slug:turnir_slug>/", views.UpdateTurnir.as_view(), name="update_turnir"),
    path("add_comand/", views.AddTeam.as_view(), name="add_comand"),
    path("update_comand/<slug:team_slug>/", views.UpdateTeam.as_view(), name="update_comand"),
    path("add_player/", views.AddPlayer.as_view(), name="add_player"),
    path("update_player/<int:player_id>", views.UpdatePlayer.as_view(), name="update_player"),
    path("turnir_details/<slug:turnir_slug>/", views.ShowTurnir.as_view(), name="show_turnir"),
    path("type_sport/<slug:sport_slug>/", views.SportType.as_view(), name="show_sport"),
    path("team/<slug:team_slug>/", views.show_team, name="show_team"),
    path("player/<int:player_id>/", views.show_player, name="show_player"),
    path("login/", views.login, name="login"),
]
