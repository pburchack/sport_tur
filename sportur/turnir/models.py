from tabnanny import verbose
from django.db import models
from datetime import date

from django.urls import reverse


class ActiveManager(models.Manager):
    """
    Custom manager for active objects
    Менеджер який вибирає всі об'єкти в яких значення
    is_active = True
    """

    def get_queryset(self):
        return super().get_queryset().filter(is_active=Turnir.Status.ACTIVE)


class Turnir(models.Model):
    class Status(models.IntegerChoices):
        ACTIVE = 1, "Турнір активний"
        INACTIVE = 0, "Турнір не активний"

    name = models.CharField(max_length=63, verbose_name="Назва турніру")
    slug = models.SlugField(max_length=63, unique=True, db_index=True)
    reglament = models.TextField(blank=True, verbose_name="Регламент")
    start_date = models.DateField(default=date.today, verbose_name="Дата початку")
    end_date = models.DateField(blank=True, null=True, verbose_name="Дата закінчення")
    is_active = models.BooleanField(
        choices=tuple(map(lambda x: (bool(x[0]), x[1]), Status.choices)), # мапимо поля класу Status в булеві значення
        default=Status.ACTIVE, verbose_name="Статус"
    )
    sport = models.ForeignKey(
        "Sport",
        on_delete=models.PROTECT,
        related_name="sport",
        verbose_name="Вид спорту",
    )
    tournament = models.ForeignKey(
        "Tournament",
        on_delete=models.PROTECT,
        related_name="tournament",
        verbose_name="Тип турніру",
    )
    teams = models.ManyToManyField(
        "Team", blank=True, related_name="teams", verbose_name="Команди учасниці"
    )

    # підключаємо менеджери до моделі
    objects = models.Manager()
    active = ActiveManager()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Турнір"
        verbose_name_plural = "Турніри"
        ordering = ["-start_date"]
        indexes = [
            models.Index(fields=["start_date"]),
        ]

    def get_absolute_url(self):
        return reverse("show_turnir", kwargs={"turnir_slug": self.slug})


class Sport(models.Model):
    name = models.CharField(max_length=63, db_index=True, verbose_name="Вид спорту")
    slug = models.SlugField(max_length=63, unique=True, db_index=True)

    class Meta:
        verbose_name = "Вид спорту"
        verbose_name_plural = "Види спорту"

    def __str__(self) -> str:
        return self.name

    def get_absolute_url(self):
        return reverse("show_sport", kwargs={"sport_slug": self.slug})


class Tournament(models.Model):
    name = models.CharField(max_length=63, db_index=True, verbose_name="Тип турніру")
    slug = models.SlugField(max_length=63, unique=True, db_index=True)

    class Meta:
        verbose_name = "Тип турніру"
        verbose_name_plural = "Типи турнірів"

    def __str__(self) -> str:
        return self.name


class Team(models.Model):
    class Status(models.IntegerChoices):
        ACTIVE = 1, "Команда активна"
        INACTIVE = 0, "Команда не активна"

    name = models.CharField(max_length=63, db_index=True, verbose_name="Назва команди")
    slug = models.SlugField(max_length=63, unique=True, db_index=True)
    city = models.CharField(max_length=63, db_index=True, verbose_name="Назва міста")
    is_active = models.BooleanField(
        choices=tuple(map(lambda x: (bool(x[0]), x[1]), Status.choices)),
        default=Status.ACTIVE, verbose_name="Статус"
    )
    players = models.ManyToManyField(
        "Player", blank=True, related_name="players", verbose_name="Гравці в команді"
    )

    class Meta:
        verbose_name = "Команда"
        verbose_name_plural = "Команди"

    def __str__(self) -> str:
        return self.name

    def get_absolute_url(self):
        return reverse("show_team", kwargs={"team_slug": self.slug})


class Player(models.Model):
    class Status(models.IntegerChoices):
        ACTIVE = 1, "Гравець активний"
        INACTIVE = 0, "Гравець не активний"

    first_name = models.CharField(max_length=63, verbose_name="Ім'я")
    last_name = models.CharField(max_length=63, db_index=True, verbose_name="Прізвище")
    patronymic = models.CharField(
        max_length=63, blank=True, null=True, verbose_name="По-батькові"
    )
    nick_name = models.CharField(
        max_length=63, blank=True, null=True, verbose_name="Нікнейм"
    )
    date_of_birth = models.DateField(
        blank=True, null=True, verbose_name="Дата народження"
    )
    is_active = models.BooleanField(
        choices=tuple(map(lambda x: (bool(x[0]), x[1]), Status.choices)),
        default=Status.ACTIVE, verbose_name="Статус"
    )

    class Meta:
        verbose_name = "Гравець"
        verbose_name_plural = "Гравці"

    def __str__(self) -> str:
        return f"{self.first_name} {self.last_name}"

    def get_absolute_url(self):
        return reverse("show_player", kwargs={"player_id": self.pk})
