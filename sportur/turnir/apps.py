from django.apps import AppConfig


class TurnirConfig(AppConfig):
    verbose_name = 'Таблиці додатку "Турнір"'
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'turnir'
