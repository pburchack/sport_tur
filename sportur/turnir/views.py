from pipes import Template
from turtle import title
from typing import Any
from django.db.models import QuerySet
from django.db.models.base import Model as Model
from django.http import Http404, HttpResponse, HttpResponseNotFound
from django.shortcuts import get_object_or_404, redirect, render
from django.template import context
from django.urls import reverse, reverse_lazy
from django.views import View
from django.views.generic import (
    DeleteView,
    DetailView,
    TemplateView,
    ListView,
    FormView,
    CreateView,
    UpdateView,
)

from .forms import AddTurnirForm, AddTeamForm, AddPlayerForm

from .models import Sport, Turnir, Team, Player

from .utils import DataMixin

menu = [
    {"title": "Статистика", "url_name": "statistic"},
    {"title": "Додати турнір", "url_name": "add_turnir"},
    {"title": "Додати команду", "url_name": "add_comand"},
    {"title": "Додати гравця", "url_name": "add_player"},
    {"title": "Вхід", "url_name": "login"},
]


class TurnirHome(DataMixin, ListView):
    template_name = "turnir/index.html"
    context_object_name = "data_db"
    title_page = "Головна сторінка"
    cat_selected = 0

    def get_queryset(self):
        all_active_turnir = (
            Turnir.active.all()
            .select_related("sport", "tournament")
            .prefetch_related("teams")
        )
        return all_active_turnir

    # all_active_turnir = (
    #     Turnir.active.all()
    #     .select_related("sport", "tournament")
    #     .prefetch_related("teams")
    # )


def statistic(request):
    return render(
        request, "turnir/statistic.html", {"title": "Статистика", "menu": menu}
    )


class ShowTurnir(DataMixin, DetailView):
    # model = Turnir
    template_name = "turnir/turnir_details.html"
    slug_url_kwarg = "turnir_slug"  # pk_url_kwarg для вибірки по ключах
    context_object_name = "turnir"

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        # context["title"] = context["turnir"].name
        # context["menu"] = menu
        return self.get_mixin_context(context, title=context["turnir"].name)

    def get_object(self, queryset=None) -> Model:
        return get_object_or_404(Turnir.active, slug=self.kwargs[self.slug_url_kwarg])


class AddTurnir(DataMixin, CreateView):
    form_class = AddTurnirForm
    template_name = "turnir/add_turnir.html"
    # якщо в моделі яка звязана з формую є метод get_absolute_url буде перенаправлення вказане в методі
    # тоді можна не вказувати success_url
    success_url = reverse_lazy("home")
    title_page = "Додати турнір"


class UpdateTurnir(DataMixin, UpdateView):
    model = Turnir
    fields = [
        "name",
        "slug",
        "sport",
        "tournament",
        "start_date",
        "end_date",
        "reglament",
        "is_active",
    ]
    template_name = "turnir/add_turnir.html"
    slug_url_kwarg = "turnir_slug"
    title_page = "Редагувати турнір"
    success_url = reverse_lazy("home")


class DeleteTurnir(DeleteView):
    pass


class AddTurnirOld(View):
    def post(self, request):
        form = AddTurnirForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("home")

        data = {
            "menu": menu,
            "title": "Додати турнір",
            "form": form,
        }

        return render(request, "turnir/add_turnir.html", data)

    def get(self, request):
        form = AddTurnirForm()

        data = {
            "menu": menu,
            "title": "Додати турнір",
            "form": form,
        }

        return render(request, "turnir/add_turnir.html", data)


class AddTeam(DataMixin, CreateView):
    form_class = AddTeamForm
    template_name = "turnir/add_team.html"
    success_url = reverse_lazy("home")
    title_page = "Додати команду"


class UpdateTeam(DataMixin, UpdateView):
    model = Team
    fields = [
        "name",
        "slug",
        "city",
        "is_active",
    ]
    template_name = "turnir/add_team.html"
    slug_url_kwarg = "team_slug"
    success_url = reverse_lazy("home")
    title_page = "Редагувати команду"


class DeleteTeam(DeleteView):
    pass


class AddPlayer(DataMixin, CreateView):
    form_class = AddPlayerForm
    template_name = "turnir/add_player.html"
    success_url = reverse_lazy("home")
    title_page = "Додати гравця"


class UpdatePlayer(DataMixin, UpdateView):
    model = Player
    fields = [
        "first_name",
        "last_name",
        "patronymic",
        "nick_name",
        "date_of_birth",
        "is_active",
    ]
    template_name = "turnir/add_player.html"
    pk_url_kwarg = "player_id"
    success_url = reverse_lazy("home")
    title_page = "Редагувати гравця"


class DeletePlayer(DeleteView):
    pass


class SportType(DataMixin, ListView):
    template_name = "turnir/index.html"
    context_object_name = "data_db"
    allow_empty = False

    def get_queryset(self):
        selected_sport = Turnir.active.filter(
            sport__slug=self.kwargs["sport_slug"]
        ).select_related("sport")
        return selected_sport

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        sport = context["data_db"][0].sport
        res = self.get_mixin_context(
            context,
            title="Вид спорту - " + sport.name,
            cat_selected=sport.pk,
        )
        return res


def show_team(request, team_slug):
    # # вибираємо команду з моделі
    team = get_object_or_404(Team, slug=team_slug)
    tournaments = Turnir.objects.filter(teams=team)

    data = {
        "title": f"Команда: {team.name}",
        "menu": menu,
        "team": team,
        "tournaments": tournaments,
        "cat_selected": None,
    }
    return render(request, "turnir/team_details.html", context=data)


def show_player(request, player_id):
    player = get_object_or_404(Player, pk=player_id)
    teams = Team.objects.filter(players=player)

    data = {
        "title": f"Гравець: {player.first_name} {player.last_name}",
        "menu": menu,
        "player": player,
        "teams": teams,
        "cat_selected": None,
    }
    return render(request, "turnir/player_details.html", context=data)


def page_not_found(request, exception):
    return HttpResponseNotFound("<h1>Сторінка не знайдена</h1>")


def login(request):
    return HttpResponse("Авторизація")
