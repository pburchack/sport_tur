INSERT INTO turnir_turnir
(id, name, slug, reglament, start_date, end_date, is_active, sport_id, tournament_id)
VALUES(1, 'Турнір 1', 'turnir-1', 'Футбол', '2024-03-18', '2024-03-20', 1, 1, 1);
INSERT INTO turnir_turnir
(id, name, slug, reglament, start_date, end_date, is_active, sport_id, tournament_id)
VALUES(2, 'Турнір 2', 'turnir-2', 'Баскетбол', '2024-03-20', '2024-03-22', 1, 2, 2);
INSERT INTO turnir_turnir
(id, name, slug, reglament, start_date, end_date, is_active, sport_id, tournament_id)
VALUES(3, 'Турнір 3', 'turnir-3', 'Волейбол', '2024-03-23', NULL, 1, 3, 1);
INSERT INTO turnir_turnir
(id, name, slug, reglament, start_date, end_date, is_active, sport_id, tournament_id)
VALUES(4, 'Турнір 4', 'turnir-4', 'Футбол', '2024-03-24', '2024-03-26', 1, 1, 1);
INSERT INTO turnir_turnir
(id, name, slug, reglament, start_date, end_date, is_active, sport_id, tournament_id)
VALUES(5, 'Турнір 5', 'turnir-5', 'Баскетбол', '2024-03-27', NULL, 1, 2, 1);
INSERT INTO turnir_turnir
(id, name, slug, reglament, start_date, end_date, is_active, sport_id, tournament_id)
VALUES(6, 'Турнір 6', 'turnir-6', 'Волейбол', '2024-03-28', '2024-03-30', 1, 3, 2);
-- https://help.pythonanywhere.com/pages/DeployExistingDjangoProject/
-- https://kosziv.pythonanywhere.com/accounts/login/?next=/catalog/