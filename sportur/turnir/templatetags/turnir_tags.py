from django import template
# from sportur import turnir
from turnir.models import Sport, Team
import turnir.views as views

# Створюємо об'єкт бібліотеки шаблонів
register = template.Library()


# Тег включення, який виводить список спортів
@register.inclusion_tag("turnir/list_sports.html")
def show_sports(cat_selected=0):
    # Отримуємо список спортів з моделі
    sports = Sport.objects.all()
    # Повертаємо словник змінних для включення у шаблон
    return {"sport": sports, "cat_selected": cat_selected}


# Тег включення, який виводить список команд
@register.inclusion_tag("turnir/list_teams.html")
def show_all_teams():
    team = Team.objects.all()
    return {"team": team}
