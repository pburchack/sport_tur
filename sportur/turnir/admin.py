from django.contrib import admin, messages
from .models import Turnir, Sport, Tournament, Team, Player


@admin.register(Turnir)
class TurnirAdmin(admin.ModelAdmin):
    # поля для редагування
    fields = [
        "name",
        "slug",
        "reglament",
        "start_date",
        "end_date",
        "sport",
        "tournament",
        "teams",
        "is_active",
    ]
    # горизонтальні бокси для перекидання команд
    filter_horizontal = ["teams"]
    # поля для відображення
    list_display = (
        "name",
        "start_date",
        "end_date",
        "sport",
        "tournament",
        "is_active",
    )
    # поля для автоматичного формування з інших полів
    prepopulated_fields = {"slug": ("name",)}
    # поля для актвації редагування
    list_display_links = ("name",)
    # сортування
    ordering = ["-start_date", "name"]
    # поля для редагування
    list_editable = ("is_active",)
    # пагінація
    list_per_page = 5
    # функції групової зміни
    actions = ["set_activate", "set_deactivate"]
    # поля для пошуку
    search_fields = ["name", "sport__name", "tournament__name"]
    # поля для фільтування
    list_filter = ["sport__name", "tournament__name", "is_active"]
    # збереження зверху
    save_on_top = True

    @admin.action(description="Активувати вибрані турніри")
    def set_activate(self, request, queryset):
        count = queryset.update(is_active=Turnir.Status.ACTIVE)
        self.message_user(request, f"{count} турнірів активовані")

    @admin.action(description="Деактивувати вибрані турніри")
    def set_deactivate(self, request, queryset):
        count = queryset.update(is_active=Turnir.Status.INACTIVE)
        self.message_user(request, f"{count} турнірів деактивовані", messages.WARNING)


@admin.register(Sport)
class SportAdmin(admin.ModelAdmin):
    list_display = ("id", "name")
    list_display_links = ("id", "name")
    prepopulated_fields = {"slug": ("name",)}


@admin.register(Tournament)
class TournamentAdmin(admin.ModelAdmin):
    list_display = ("id", "name")
    list_display_links = ("id", "name")
    prepopulated_fields = {"slug": ("name",)}


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    fields = ("name", "city", "slug", "players", "is_active")
    filter_horizontal = ["players"]
    list_display = ("id", "name", "city", "is_active")
    list_display_links = ("id", "name")
    list_editable = ("is_active",)
    actions = ["set_activate", "set_deactivate"]
    search_fields = ["name", "city"]
    list_filter = ["city", "is_active"]
    prepopulated_fields = {
        "slug": (
            "name",
            "city",
        )
    }

    @admin.action(description="Активувати вибрані команди")
    def set_activate(self, request, queryset):
        count = queryset.update(is_active=Team.Status.ACTIVE)
        self.message_user(request, f"{count} команди активовані")

    @admin.action(description="Деактивувати вибрані команди")
    def set_deactivate(self, request, queryset):
        count = queryset.update(is_active=Team.Status.INACTIVE)
        self.message_user(request, f"{count} команди деактивовані", messages.WARNING)


@admin.register(Player)
class PlayerAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "first_name",
        "last_name",
        "nick_name",
        "is_active",
        "get_teams",
    )
    list_display_links = ("id", "first_name", "last_name", "nick_name")
    ordering = ("last_name", "first_name")
    list_editable = ("is_active",)
    list_per_page = 5
    actions = ["set_activate", "set_deactivate"]
    search_fields = ["last_name", "first_name", "nick_name"]
    list_filter = ["is_active"]

    @admin.action(description="Активувати вибраних гравців")
    def set_activate(self, request, queryset):
        count = queryset.update(is_active=Player.Status.ACTIVE)
        self.message_user(request, f"{count} гравців активовані")

    @admin.action(description="Деактивувати вибраних гравців")
    def set_deactivate(self, request, queryset):
        count = queryset.update(is_active=Player.Status.INACTIVE)
        self.message_user(request, f"{count} гравців деактивовано", messages.WARNING)

    @admin.display(description="Команди")
    def get_teams(self, obj):
        return ", ".join([team.name for team in obj.players.all()])
