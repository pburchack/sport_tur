from cProfile import label
from django import forms
from django.core.validators import MaxLengthValidator, MinLengthValidator
from django.forms import widgets
from django.utils.deconstruct import deconstructible
from .models import Tournament, Sport, Turnir, Team, Player


# Приклад простого класу валідатора
@deconstructible()
class UkranianValidator:
    ALLOWED_CHARS = (
        "АаБбВвГгҐґДдЕеЄєЖжЗзИиІіЇїЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщьЮюЯя0123456789- "
    )
    code = "ukranian"

    def __init__(self, message=None):
        self.message = (
            message if message else "Тільки літери української абетки, дефіс і пробіл"
        )

    def __call__(self, value, *args, **kwargs):
        if not (set(value) <= set(self.ALLOWED_CHARS)):
            raise forms.ValidationError(self.message, code=self.code)


class AddTurnirForm(forms.ModelForm):
    # перевизначення(override) потрібних полів
    sport = forms.ModelChoiceField(
        label="Спорт", empty_label="Вид спорту", queryset=Sport.objects.all()
    )
    tournament = forms.ModelChoiceField(
        label="Турнір", empty_label="Тип турніру", queryset=Tournament.objects.all()
    )

    # формат вводу дати "DD.MM.YYYY".

    class Meta:
        model = Turnir
        fields = [
            "name",
            "slug",
            "sport",
            "tournament",
            "start_date",
            "end_date",
            "reglament",
            "is_active",
        ]
        widgets = {
            "name": forms.TextInput(attrs={"class": "form-input"}),
            "reglament": forms.Textarea(attrs={"cols": 50, "rows": 5}),
        }
        labels = {"slug": "URL"}

    # приклад валідатора як методу, для одноразової перевірки для поля "name"
    # def clean_name(self):
    #     name = self.cleaned_data["name"]
    #     ALLOWED_CHARS = "АаБбВвГгҐґДдЕеЄєЖжЗзИиІіЇїЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщьЮюЯя0123456789- "

    #     if not (set(name) <= set(ALLOWED_CHARS)):
    #         raise forms.ValidationError(
    #             "Тільки літери української абетки, дефіс і пробіл"
    #         )
    #     return name


class AddTeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = [
            "name",
            "slug",
            "city",
            "is_active",
        ]
        widgets = {
            "name": forms.TextInput(attrs={"class": "form-input"}),
            "city": forms.TextInput(attrs={"class": "form-input"}),
        }
        labels = {"slug": "URL"}


class AddPlayerForm(forms.ModelForm):
    class Meta:
        model = Player
        fields = [
            "first_name",
            "last_name",
            "patronymic",
            "nick_name",
            "date_of_birth",
            "is_active",
        ]
        widgets = {
            "first_name": forms.TextInput(attrs={"class": "form-input"}),
            "last_name": forms.TextInput(attrs={"class": "form-input"}),
            "patronymic": forms.TextInput(attrs={"class": "form-input"}),
        }
